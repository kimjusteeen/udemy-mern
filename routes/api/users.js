const express = require('express');
const router = express.Router()
const {check, validationResult} = require('express-validator/check');
const gravatar= require('gravatar');
const bcrypt = require('bcryptjs')

const User = require('../../models/User.js')

// @route POST api/users
// @desc Register User
// @acess Public

let validations = [
    check('name', 'Name is required').not().isEmpty(),
    check('email', 'Please include a valid email').isEmail(),
    check('password', 'Please enter a password with 6 or more character').isLength({min: 6}),
];

router.post('/', validations, async (req, res) => {
    const errors = validationResult(req);

    if(!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()})
    }

    const {name, email, password} = req.body;

    try {

    //see if user exists
    let user = await User.findOne({ email });

    if(user){
        return res.status(400).json({ errors:[ {msg: 'User already exists'} ] });
    }

    const avatar = gravatar.url(email, {
        s: '200',
        r: 'pg',
        d: 'mm'
    });
    
    //get user
    user = new User({
        name,
        email,
        avatar,
        password
    });

    //encrypt password using bcrypt
    const salt = await bcrypt.genSalt(10);

    user.password = await bcrypt.hash(password, salt);

    await user.save();

    //return json webtoken
    
    res.send('User registered');

    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }


});

module.exports = router;